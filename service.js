/**
 * Created by pengxiankaikai on 2018/3/2.
 */

var http = require("http");
var url = require("url");

function start(route, handles) {

    function onRequest(request, response) {

        var uri = request.url;

        var pathname = url.parse(uri).pathname;
        console.log("pathname = " + pathname);

        var param = url.parse(uri).query;
        console.log("param = " + param);

        route(handles, pathname, response, request);
    }

    var server = http.createServer(onRequest);

    server.listen(8888);

    console.log("server has started");
}

exports.start = start;