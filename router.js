/**
 * Created by pengxiankaikai on 2018/3/2.
 */

function route(handles, pathname, response, request) {
    console.log("this route = " + pathname);
    console.log("typeof handles[pathname] : " + typeof handles[pathname]);
    if (typeof handles[pathname] == 'function'){
        return handles[pathname](response, request);
    }else {
        console.log("404");
        response.writeHead(404, {"Content-Type": "text/plain"});
        response.write("404 NOT FOUND");
        response.end();
    }
}

exports.route = route;