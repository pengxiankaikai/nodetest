/**
 * Created by pengxiankaikai on 2018/3/2.
 */


var queryString = require("querystring"),
    fs = require("fs"),
    formidable = require("formidable"),
    iconv = require("iconv-lite"),
    util = require("util");

function start(response) {
    console.log("handler start");

    // var exec = require("child_process").exec;
    // // sleep(10000);
    // exec("find /", { timeout: 10000, maxBuffer: 20000*1024 }, function (error, stdout, stderr) {
    //
    //     response.writeHead(200, {"Content-Type": "text/plain"});
    //     response.write(stdout);
    //     response.end();
    // })

    var body = '<html>'+
        '<head>'+
        '<meta http-equiv="Content-Type" content="text/html; '+
        'charset=UTF-8" />'+
        '</head>'+
        '<body>'+
        '<form action="/upload" enctype="multipart/form-data" method="post">'+
        // '<textarea name="text" rows="20" cols="60"></textarea>'+
        '<input type="file" name="upload" multiple="multiple"/>'+
        '<input type="submit" value="upload" />'+
        '</form>'+
        '</body>'+
        '</html>';
        response.writeHead(200, {"Content-Type": "text/html"});
        response.write(body);
        response.end();
}

function upload(response, request) {
    console.log("handler upload");
    var form = new formidable.IncomingForm();

    form.parse(request, function (error, fields, files) {
        console.log("parsing done ===== " + files.upload.path);
        fs.renameSync(files.upload.path, "/tmp/test.png");
        response.writeHead(200, {"Content-Type": "text/html"});
        response.write("this img : <br/> ");
        response.write("<img src='/show'/>")
        response.end(util.inspect({fields: fields, files: files}));
    })
}

function show(response, request) {
    console.log("into show method");
    fs.readFile("/tmp/test.png", "binary", function (error, file) {
        console.log("error: " + error);
        if (error){
            response.writeHead(500, {"Content-Type": "text/plain"});
            response.write(error + "\n");
            response.end();
        }else {
            // file = iconv.decode(file, "gbk");
            response.writeHead(200, {"Content-Type": "image/png"});
            response.write(file, "binary");
            response.end();
        }
    })
}

function sleep(milliSeconds) {
    var startTime = new Date().getTime();
    while (new Date().getTime() < startTime + milliSeconds);
}

function changeEncode(response) {
    var str = "我是彭凯，hello";
    console.log(str);
    str = iconv.encode(str, "gbk");
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.write(str);
    response.end();
}

exports.start = start;
exports.upload = upload;
exports.show = show;
exports.changeEncode = changeEncode;