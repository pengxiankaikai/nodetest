/**
 * Created by pengxiankaikai on 2018/3/2.
 */

var server = require("./service");
var router = require("./router");
var requestHandlers = require("./requestHandlers");
var formidable = require("formidable");

var handlers = {};
handlers["/"] = requestHandlers.start;
handlers["/start"] = requestHandlers.start;
handlers["/upload"] = requestHandlers.upload;
handlers["/show"] = requestHandlers.show;
handlers["/change"] = requestHandlers.changeEncode;

server.start(router.route, handlers);